'use strict';
// const fs = require("fs");
const request = require("request-promise")

const BASE_URI = 'https://accounts.us1.gigya.com'
const SECRET_KEY = encodeURIComponent("WeG9d7flDE8InTceFvGfV3CYfk03jHXUcmoj81dU0Ds=")
const API_KEY = encodeURIComponent("3_ClDcX23A7tU8pcydnKyENXSYP5kxCbwH4ZO741ZOujPRY8Ksj2UBnj8Zopb0OX0K")
const QUERY_STRING = `apiKey=${API_KEY}&secret=${SECRET_KEY}`
const LOGIN_ENDPOINT = "accounts.login"

let html;

function getParams(body) {
  return body.split("&").reduce((acc, param) => {
    const kv = param.split("=")
    return {...acc, [kv[0]]: kv[1] }
  }, {})
}

function login(event, _, callback) {
  const { redirect_uri: redirectURI, state } = event.queryStringParameters
  console.log('====================================');
  console.log(redirectURI);
  console.log('====================================');
  // html = html || fs.readFileSync("login.html", "utf8")
  callback(null, {
    statusCode: 200,
    headers: { "Content-Type": "text/html" },
    body: `
      <form action="/dev/gigya/authenticate" method="post">
        <div>
          <label for="mail">E-mail:</label>
          <input type="email" id="mail" name="email">
        </div>
        <div>
          <label for="password">Password</label>
          <input type="password" id="password" name="password">
        </div>
        <input type="hidden" name="redirectURI" value="${redirectURI}">
        <input type="hidden" name="state" value="${state}">
        <div>
          <button type="submit">Submit</button>
        </div>
      </form>
    `,
  })
}

async function authenticate(event, _, callback) {
  const { email, password, redirectURI, state } = getParams(event.body)
  // const pass = encodeURIComponent(password)
  const credentials = `loginID=${email}&password=${password}`
  const url = `${BASE_URI}/${LOGIN_ENDPOINT}?${QUERY_STRING}&${credentials}`
  console.log('====================================');
  console.log(url);
  console.log('====================================');
  let res = await request.post(url)
  const { UID } = JSON.parse(res)
  const redirect = decodeURIComponent(redirectURI)
  const Location = `${redirect}#state=${state}&access_token=${UID}&token_type=Bearer`
  console.log('====================================');
  console.log(Location);
  console.log('====================================');
  callback(null, {
    statusCode: 301,
    headers: { Location }
  })
}

module.exports = {
  login,
  authenticate
}
